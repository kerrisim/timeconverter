<?php

//Daylight savings time +1

 $timezones = array(
								"Australian Central Standard Time" => 9.5,
								"ASEAN Common Time"  => 8,
								"Australian Eastern Standard Time"  => 10,
								"Alaska Standard Time" => -9,
								"Atlantic Daylight Time" => -3,
								"Afghanistan Time" => 4.5,
								"Amazon Time" => -4,
								"Armenia Time" => 4,
								"Argentina Time" => -3,
								"Arabia Standard Time" => 3,
								"Atlantic Standard Time" => -4,
								"Australian Western Standard Time" => 8,
								"Azores Standard Time" => -1,
								"Azerbaijan Time" => 4,
								"Brunei Time" => 8,
								"British Indian Ocean Time" => 6,
								"Baker Island Time" => -12,
								"Bolivia Time" => -4,
								"Brasilla Time" => -3,
								"Bangladesh Standard Time" => 6,
								"Bhutan Time" => 6,
								"Central Africa Time" => 2,
								"Cocos Islands Time" => 6.5,
								"Central European Time" => 1,
								"Chatham Standard Time" => 12.7,
								"Choibalsan" => 8,
								"Chamorro Standard Time" => 10,
								"Chuuk Time" => 10,
								"Clipperton Island Standard Time" => -8,
								"Central Indonesia Time" => 8,
								"Cook Island Time" => -10,
								"Chile Standard Time" => -4,
								"Colombia Time" => -5,
								"Central Standard Time (North America)" => -6,
								"China Standard Time" => 8,
								"Central Standard Time (Australia)" => 9.5,
								"Cuba Standard Time" => -5,
								"China Time" => 8,
								"Cape Verde Time" => -1,
								"Christmas Island Time" => 7,
								"Davis Time" => 7,
								"Easter Island Standard Time" => -6,
								"East Africa Time" => 3,
								"Ecuador Time" => -5,
								"Eastern European Time" => 2,
								"Eastern Greenland Time" => -1,
								"Eastern Indonesian Time" => 9,
								"Eastern Standard Time (North America)" => -5,
								"Fiji Time" => 12,
								"Falkland Islands Time" => -4,
								"Georgia Standard Time" => 4,
								"Greenwich Mean Time" => 0,
								"Gulf Standard Time" => 4,
								"Hong Kong Time" => 8,
								"Hawaii Standard Time" => -10,
								"Iran Standard Time" => 3.5,
								"Indian Standard Time" => 5.5,
								"Irish Standard Time" => 1,
								"Israel Standard Time" => 2,
								"Japan Standard Time" => 9,
								"Lord Howe Standard Time" => 10.5,
								"Malaysia Standard Time" => 8,
								"Mountain Standard Time (North America)" => -7,
								"Myanmar Standard Time" => 6.5,
								"Newfoundland Standard Time" => 3.5,
								"New Zealand Standard Time" => 12, 
								"Pakistan Standard Time" => 5,
								"Saint Pierre and Miquelon Standard Time" => -3,
								"Pohnpei Standard Time" => 11,
								"Pacific Standard Time (North America)" => -8,
								"Samoa Standard Time" => -11,
								"Singapore Standard Time" => 8,
								"Thailand Standard Time" => 7,
								"Uruguay Standard Time" => -3,
								"Venezuelan Standard Time" => -4.5,
								"West Africa Time" => 1,
								"Western European Time" => 0
								);

	function populateTimeZones()
	{
		global $timezones;
		echo "FUNCTION";
		foreach ($timezones as $zone => $v)
    	{

    		echo "<option value=\"".$v."\">".$zone."</option>";
    	}
			
	}

	function convertTime($timeHour, $timeMin, $fromTimezone, $toTimezone, $fromDst, $toDst)
	{
		$nextDay = false;
		$pastDay = false;
		$offset = $fromTimezone - $toTimezone; 

		if($fromDst)
		{
			$timeHour = $timeHour - 1;
		}

		//If the offset concerns minutes ie. 10:30
		if(is_float($offset))
		{
			$outputHour = $timeHour - floor($offset) - 1;
			$outputMin = $timeMin - 30;

			if($outputMin < 0)
			{
				$outputMin = 60 - abs($outputMin);
			}
		}
		else
		{
			//Minutes remain the same, just change the hour
			$outputHour = $timeHour - $offset;
			$outputMin = $timeMin;
		}

		//If it goes into the day after or before
		if($outputHour > 24)
		{
			//Next day
			$outputHour = $outputHour - 24;
			$nextDay = true;
			
		}
		else if($outputHour < 0)
		{
			//Day before
			$outputHour = 24 - abs($outputHour);
			$pastDay = true;
		}

		if($toDst)
		{
			$outputHour = $outputHour + 1;
		}

		//Echo out results
		if($nextDay)
		{
			echo "Next Day ".$outputHour.":".$outputMin;
		}
		else if($pastDay)
		{
			echo "Day Before ".$outputHour.":".$outputMin;
		}
		else
		{
			echo "Same Day ".$outputHour.":".$outputMin;
		}
	}


	














?>