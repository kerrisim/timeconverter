  function validInput(form)
  {
  	var validInputPattern = /^(0|[1-9][0-9]*)$/;

//Check if input is only numbers and only two digits are allowed
  	if(form.hour.value == "" || form.min.value == "")
  	{
  		alert("Time Values are empty.");
  		return false;
  	}
  	if(form.hour.value > 23 || form.hour.value < 0 || form.min.value > 59 || form.min.value < 0)
  	{
  		alert("Time input is not valid.");
  		return false;
  	}

    if(validInputPattern.test(form.hour.value) || validInputPattern.test(form.min.value))
    {
      return true;
    }
    else 
    {
      alert("Time input is not valid. Only numbers are allowed.");
      return false;
    }
    return true;
  
  }