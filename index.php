<html>

	<head>
		<title></title>
		<link rel="stylesheet" type="text/css" href="main.css">
		<script type="text/JavaScript" src="validityFunctions.js"></script> 
		<?php include 'convertFunctions.php';?>
	</head>

	<body>
		
	<div id="centrepiece">
	<h1>Time Conversion</h1>
	<h3>Enter the time for conversion in 24 hour format.</h3>

	<form method="post" Action="index.php" OnSubmit="return validInput(this)">
		Hour: <input type="text" name="hour">
		Minutes: <input type="text" name="min"> <br>
		From Timezone: <select name="fromTimezone"><?php populateTimeZones(); ?></select> 
		<input type="checkbox" name="fromDst" value="true">Daylight Saving Time </input> <br>
		To Timezone:  <select name="toTimezone"><?php populateTimeZones(); ?></select>
		<input type="checkbox" name="toDst" value="true">Daylight Saving Time </input><br>
		<input type="Submit" Value="Submit">
	 </form>

		<!--Output of conversion -->

		

	<?php
		if(!empty($_POST["hour"]))
		{
			if(empty($_POST["fromDst"]) && empty($_POST["toDst"]))
			{
				convertTime($_POST["hour"],$_POST["min"],$_POST["fromTimezone"],$_POST["toTimezone"],false, false);
			}
			else if(!empty($_POST["fromDst"]) && empty($_POST["toDst"]))
			{
				convertTime($_POST["hour"],$_POST["min"],$_POST["fromTimezone"],$_POST["toTimezone"],true, false);
			}
			else if(empty($_POST["fromDst"]) && !empty($_POST["toDst"]))
			{
				convertTime($_POST["hour"],$_POST["min"],$_POST["fromTimezone"],$_POST["toTimezone"],false, true);
			}
			else
			{
				convertTime($_POST["hour"],$_POST["min"],$_POST["fromTimezone"],$_POST["toTimezone"],true, true);
			}
			
			//echo "<p id=\"outputTime\">".$_POST["hour"].$_POST["min"]." </p>";
		}
	?>
		
	</div>
	</body>

</html>